// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DNArt
{
	[Register ("HomePage")]
	partial class HomePage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ContentView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView MainView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ContentView != null) {
				ContentView.Dispose ();
				ContentView = null;
			}
			if (MainView != null) {
				MainView.Dispose ();
				MainView = null;
			}
		}
	}
}
