using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DNArt
{
	partial class NavBarHome : UINavigationBar
	{
		public NavBarHome (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 25.5, 6, 51, 28));
            centerlogo.Image = UIImage.FromFile("logo.png");

            UIImageView separator = new UIImageView(new CGRect(0, 36, this.Frame.Width, 8));
            separator.Image = UIImage.FromFile("NavShade.png");

            AddSubview(centerlogo);
            AddSubview(separator);
        }

    }
}
