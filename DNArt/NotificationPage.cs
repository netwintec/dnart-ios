using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Threading;
using UIKit;

namespace DNArt
{
	partial class NotificationPage : UIViewController
	{

        double Px, Py, Height, Widht;
        int yy;
        UIScrollView scrollView;
        nfloat ViewWidht;

        public NotificationPage (IntPtr handle) : base (handle)
		{
		}
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewWidht = View.Frame.Width;

            yy = 0;
            //NSUserDefaults.StandardUserDefaults.SetString(token, "TokenRoccoN4U");

            bool isPushNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isDNArtPushNotification");
            bool isBeaconNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isDNArtBeaconNotification");

            if (isPushNotification != null && isPushNotification == true)
            {

                NSUserDefaults.StandardUserDefaults.SetBool(false, "isDNArtPushNotification");
            }

            if (isBeaconNotification != null && isBeaconNotification == true)
            {

                NSUserDefaults.StandardUserDefaults.SetBool(false, "isDNArtBeaconNotification");
            }




            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentPage.Frame.Height));
			scrollView.BackgroundColor = UIColor.Clear;
			ContentPage.BackgroundColor = UIColor.Clear;

            UIStoryboard storyboard = new UIStoryboard();

            storyboard = UIStoryboard.FromName("Main", null);
            Widht = (View.Frame.Width - 60) / 2;
            Height = Widht;
            Px = 20;
            Py = 20;


            UIImageView imageView = new UIImageView(new CGRect(ViewWidht / 2 - 50, 20, 150, 82));
            imageView.Image = UIImage.FromFile("placeholder.png");

            yy += 102;

            UILabel Label = new UILabel();

            var size = UIStringDrawing.StringSize(NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotificationDNArt"), UIFont.FromName("Roboto-Regular", 16), new CGSize(ContentPage.Frame.Width-60, 2000));
            Console.WriteLine("4:" + size.Height + "  " + size.Width);

            if (NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotificationDNArt").CompareTo("null") == 0)
            {

                Label = new UILabel(new CGRect(30, 20, ContentPage.Frame.Width-60, size.Height));
                yy -= 102;
            }
            else
            {
                Label = new UILabel(new CGRect(30, yy + 20, ContentPage.Frame.Width - 60, size.Height));
            }
            Label.Text = NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotificationDNArt");
            Label.Font = UIFont.FromName("Roboto-Regular", 16);
            Label.TextAlignment = UITextAlignment.Center;
            Label.Lines = 0;
            yy += (int)(size.Height + 20);


			Console.WriteLine("Text:"+NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotificationDNArt"));
            Console.WriteLine("Image:" + NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotificationDNArt") + "|" + NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotificationDNArt").CompareTo("null"));

            if (NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotificationDNArt").CompareTo("null") != 0)
                scrollView.Add(imageView);

            scrollView.Add(Label);


            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 20);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            ContentPage.Add(scrollView);

            if (NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotificationDNArt").CompareTo("null") != 0)
                SetImageAsync(imageView, Label);
        }


        public void SetImageAsync(UIImageView image, UILabel label)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(NSUserDefaults.StandardUserDefaults.StringForKey("ImageNotificationDNArt")) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        Console.WriteLine(w + "  " + h);
                        image.Frame = new CGRect(20, 20, View.Frame.Width - 40, ((View.Frame.Width - 40) * h) / w);
                        Double yyy = ((View.Frame.Width - 40) * h) / w + 20;


                        var size = UIStringDrawing.StringSize(NSUserDefaults.StandardUserDefaults.StringForKey("DescriptionNotificationDNArt"), label.Font, new CGSize(200, 2000));
                        Console.WriteLine("4:" + size.Height + "  " + size.Width);

                        label.Frame = new CGRect(ViewWidht / 2 - 100, yyy + 20, 200, size.Height);
                        yyy += (int)size.Height + 20;

                        scrollView.ContentSize = new CGSize(View.Frame.Width, yyy + 20);
                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }


        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }
    }
}
