// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DNArt
{
	[Register ("NotificationPage")]
	partial class NotificationPage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ContentPage { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ContentPage != null) {
				ContentPage.Dispose ();
				ContentPage = null;
			}
		}
	}
}
