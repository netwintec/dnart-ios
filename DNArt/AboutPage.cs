﻿using Carousels;
using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace DNArt
{
    partial class AboutPage : UIViewController
    {

        const string TEXT_ABOUT= "Ogni giorno in DNArt c’è qualche progetto in costruzione, diversi cantieri creativi “aperti” ed un via-vai di “betoniere” piene di idee e strategie da sviluppare.\n"+
            "E’ così da dieci anni: progettiamo e costruiamo comunicazione per chi progetta e costruisce case, quartieri, città, affiancando e lavorando per chi produce prodotti e soluzioni efficaci per costruire bene, in modo sostenibile, duraturo, confortevole. E per questo siamo sempre stati convinti che guardare con nuovi occhi tutto quello che accade ogni giorno sia il presupposto indispensabile per conseguire risultati duraturi: costruire altro per andare oltre.\n"+
            "Per caso, abbiamo detto che qui si costruisce...? Other construction, better communication.";

        const string TEXT_ABOUT_BOLD = "UNDER CONSTRUCTION, da sempre per il futuro.";
        //Via A. Barazzuoli 21, 50136 Firenze (Italia)
        const string TEXT_INDIRIZZO = "Via A. Barazzuoli, 21\n" +
            "50136 Firenze - ITALY\n"+
            "P.I/C.F.: IT 05420390485";
        const string TEXT_TELEFONO = "Tel +39 055 55.20.650";
        const string TEXT_FAX = "Fax +39 055 50.02.090";
        const string TEXT_INFO = "info@dnartstudio.it";

        List<string> banner = new List<string>();

        public AboutPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UILabel titolo = new UILabel(new CGRect(30,15,ContentView.Frame.Width-60,20));
            titolo.Text = "ABOUT";
            titolo.TextColor = UIColor.Black;
            titolo.Font = UIFont.FromName("Roboto-Bold",20);

            UIView separator = new UIView(new CGRect(30,40, ContentView.Frame.Width - 60, 4));
            separator.BackgroundColor = UIColor.Black;

            banner.Add("banner_1.jpg");
            banner.Add("banner_2.jpg");
            banner.Add("banner_3.jpg");
            banner.Add("banner_4.jpg");
            banner.Add("banner_5.jpg");
            banner.Add("banner_6.jpg");
            banner.Add("banner_7.jpg");
            banner.Add("banner_8.jpg");
            banner.Add("banner_9.jpg");
            banner.Add("banner_10.jpg");

            
            iCarousel carousel = new iCarousel(new CGRect(0, 60, ContentView.Frame.Width, ContentView.Frame.Width/2));
            carousel.Type = iCarouselType.Rotary;
            carousel.DataSource = new CarouselDataSource(banner.Count,(float)ContentView.Frame.Width, (float)ContentView.Frame.Width/2,banner);
            carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

            int yy = 0;

            UIScrollView scrollview = new UIScrollView(new CGRect(0, (60 + (ContentView.Frame.Width / 2) + 15), ContentView.Frame.Width, ContentView.Frame.Height - (60 + (ContentView.Frame.Width / 2) + 15)));

            var size = UIStringDrawing.StringSize(TEXT_ABOUT_BOLD, UIFont.FromName("Roboto-Bold", 16), new CGSize(ContentView.Frame.Width-60, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            UILabel tit = new UILabel(new CGRect(30,yy,ContentView.Frame.Width-60,size.Height));
            tit.Text = TEXT_ABOUT_BOLD;
            tit.TextColor = UIColor.Black;
            tit.Font = UIFont.FromName("Roboto-Bold",16);
            tit.Lines = 0;

            yy += (int)size.Height;

            size = UIStringDrawing.StringSize(TEXT_ABOUT, UIFont.FromName("Roboto-Regular", 15), new CGSize(ContentView.Frame.Width - 60, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            UILabel text = new UILabel(new CGRect(30, yy+10, ContentView.Frame.Width-60, size.Height));
            text.Text = TEXT_ABOUT;
            text.TextColor = UIColor.Black;
            text.Font = UIFont.FromName("Roboto-Regular", 15);
            text.Lines = 0;

            yy += (int)(size.Height+10);

            size = UIStringDrawing.StringSize(TEXT_INDIRIZZO, UIFont.FromName("Roboto-Bold", 15), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            float widht = (float)size.Width;

            UIImageView home = new UIImageView(new CGRect((ContentView.Frame.Width/2)-((35+widht)/2),(yy+15)+(size.Height/2)-15,30,30));
            home.Image = UIImage.FromFile("home.png");

            UILabel indirizzo = new UILabel(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2) + 35, yy + 15, widht, size.Height));
            indirizzo.Text = TEXT_INDIRIZZO;
            indirizzo.TextColor = UIColor.Black;
            indirizzo.Font = UIFont.FromName("Roboto-Bold", 15);
            indirizzo.Lines = 0;

            yy += (int)(15 + size.Height);

            size = UIStringDrawing.StringSize(TEXT_TELEFONO, UIFont.FromName("Roboto-Bold", 15), new CGSize(widht, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            UIImageView tel = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2), (yy + 15) + (size.Height / 2) - 15, 30, 30));
            tel.Image = UIImage.FromFile("ico_telefono.png");

            UILabel telefono = new UILabel(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2) + 35, yy + 15, widht, size.Height));
            telefono.Text = TEXT_TELEFONO;
            telefono.TextColor = UIColor.Black;
            telefono.Font = UIFont.FromName("Roboto-Bold", 15);
            telefono.Lines = 0;

            yy += (int)(15 + size.Height);

            size = UIStringDrawing.StringSize(TEXT_FAX, UIFont.FromName("Roboto-Bold", 15), new CGSize(widht, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            UIImageView fax = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2), (yy + 15) + (size.Height / 2) - 15, 30, 30));
            fax.Image = UIImage.FromFile("ico_fax.png");

            UILabel FaxText = new UILabel(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2) + 35, yy + 15, widht, size.Height));
            FaxText.Text = TEXT_FAX;
            FaxText.TextColor = UIColor.Black;
            FaxText.Font = UIFont.FromName("Roboto-Bold", 15);
            FaxText.Lines = 0;

            yy += (int)(15 + size.Height);

            size = UIStringDrawing.StringSize(TEXT_INFO, UIFont.FromName("Roboto-Bold", 15), new CGSize(widht, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            UIImageView mail = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2), (yy + 15) + (size.Height / 2) - 15, 30, 30));
            mail.Image = UIImage.FromFile("ico_mail.png");

            UILabel info = new UILabel(new CGRect((ContentView.Frame.Width / 2) - ((35 + widht) / 2) + 35, yy + 15, widht, size.Height));
            info.Text = TEXT_INFO;
            info.TextColor = UIColor.Black;
            info.Font = UIFont.FromName("Roboto-Bold", 15);
            info.Lines = 0;

            yy += (int)(15 + size.Height);

            scrollview.ContentSize= new CGSize(ContentView.Frame.Width, yy + 10);

            scrollview.Add(tit);
            scrollview.Add(text);
            scrollview.Add(home);
            scrollview.Add(indirizzo);
            scrollview.Add(tel);
            scrollview.Add(telefono);
            scrollview.Add(fax);
            scrollview.Add(FaxText);
            scrollview.Add(mail);
            scrollview.Add(info);

            ContentView.Add(titolo);
            ContentView.Add(separator);
            ContentView.Add(carousel);
            ContentView.Add(scrollview);
        }
    }

    public class CarouselDataSource : iCarouselDataSource
    {
        int[] items;
        float Widht, Height;
        List<string> listImg;

        public CarouselDataSource(int number, float w, float h, List<string> lI)
        {
            // create our amazing data source
            items = Enumerable.Range(number, number).ToArray();
            Widht = w;
            Height = h;
            listImg = lI;
        }

        // let the carousel know how many items to render
        public override nint GetNumberOfItems(iCarousel carousel)
        {
            // return the number of items in the data
            return items.Length;
        }

        // create the view each item in the carousel
        public override UIView GetViewForItem(iCarousel carousel, nint index, UIView view)
        {
            UIImageView imageView = null;
            UIView View = null;

            if (view == null)
            {

                View = new UIView(new CGRect(0, 0, Widht, Height));

                imageView = new UIImageView(new CGRect(0, 0, Widht, Height));
                imageView.Tag = 2;

                UIImage cachedImage = null;

                cachedImage = UIImage.FromFile(listImg[(int)index]);
                imageView.Image = cachedImage;

                View.AddSubview(imageView);
            }
            else
            {
                // get a reference to the label in the recycled view
                View = view;
                imageView = (UIImageView)view.ViewWithTag(2);
                UIImage cachedImage = null;
                cachedImage = UIImage.FromFile(listImg[(int)index]);
                imageView.Image = cachedImage;

                //label = (UILabel)view.ViewWithTag(1);
            }

            // set the values of the view
            return View;
        }
    }
}
