using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DNArt
{
	partial class HomePage : UIViewController
	{

        float heightImage, widthImage, heightText, widthText;
        float PaddingX, PaddingY;

        public string url;

		public HomePage (IntPtr handle) : base (handle)
		{
		}

        public static HomePage Instance { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            bool isPushNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isDNArtPushNotification");
            bool isBeaconNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isDNArtBeaconNotification");
            Console.WriteLine("PUSH:"+isPushNotification + "|BEACON:" + isBeaconNotification);

            if (isPushNotification == true)
            {
                UIStoryboard storyboard = new UIStoryboard();
                storyboard = UIStoryboard.FromName("Main", null);
                UIViewController lp = storyboard.InstantiateViewController("Notification");
                this.PresentModalViewController(lp, true);

            }

            if (isBeaconNotification == true)
            {
                UIStoryboard storyboard = new UIStoryboard();
                storyboard = UIStoryboard.FromName("Main", null);
                UIViewController lp = storyboard.InstantiateViewController("Notification");
                this.PresentModalViewController(lp, true);
            }


            if (!isBeaconNotification && !isPushNotification)
            {

                HomePage.Instance = this;

                widthImage = (float)((MainView.Frame.Width - 90) / 2);
                heightImage = (float)((267 * widthImage) / 280);
                widthText = (float)((MainView.Frame.Width - 90) / 2);
                heightText = 20;
                PaddingX = 30;
                PaddingY = (float)((MainView.Frame.Height - ((heightText + heightImage) * 2)) / 3);

                UIStoryboard storyboard = new UIStoryboard();
                storyboard = UIStoryboard.FromName("Main", null);
                UIViewController ap = storyboard.InstantiateViewController("AboutPage");
                UIViewController wp = storyboard.InstantiateViewController("WebPage");

                Console.WriteLine(MainView.Frame.Width + "|" + MainView.Frame.Height + "|" + widthImage + "|" + heightImage + "|" + widthText + "|" + heightText + "|" + PaddingX + "|" + PaddingY);

                UIButton AboutButton = new UIButton(new CGRect(PaddingX, PaddingY, widthImage, heightImage));
                UIImageView AboutImage = new UIImageView(AboutButton.Bounds);
                AboutImage.Image = UIImage.FromFile("about.png");
                AboutButton.AddSubview(AboutImage);
                AboutButton.TouchUpInside += (object sender, EventArgs e) =>
                {

                    this.PresentModalViewController(ap, false);

                };

                UIButton StrategiaButton = new UIButton(new CGRect(PaddingX + PaddingX + widthImage, PaddingY, widthImage, heightImage));
                UIImageView StrategiaImage = new UIImageView(StrategiaButton.Bounds);
                StrategiaImage.Image = UIImage.FromFile("STRATEGIA.png");
                StrategiaButton.AddSubview(StrategiaImage);
                StrategiaButton.TouchUpInside += (object sender, EventArgs e) =>
                {

                    url = "http://www.dnartstudio.it/strategie.html";
                    this.PresentModalViewController(wp, false);

                };

                UILabel AboutText = new UILabel(new CGRect(PaddingX, PaddingY + heightImage, widthText, heightText));
                AboutText.Text = "ABOUT";
                AboutText.Font = UIFont.FromName("Roboto-Bold", 17);
                AboutText.TextColor = UIColor.Black;
                AboutText.TextAlignment = UITextAlignment.Center;

                UILabel StrategiaText = new UILabel(new CGRect(PaddingX + PaddingX + widthText, PaddingY + heightImage, widthText, heightText));
                StrategiaText.Text = "STRATEGIA";
                StrategiaText.Font = UIFont.FromName("Roboto-Bold", 17);
                StrategiaText.TextColor = UIColor.Black;
                StrategiaText.TextAlignment = UITextAlignment.Center;

                UIButton AdvertisingButton = new UIButton(new CGRect(PaddingX, PaddingY + PaddingY + heightImage + heightText, widthImage, heightImage));
                UIImageView AdvertisingImage = new UIImageView(AdvertisingButton.Bounds);
                AdvertisingImage.Image = UIImage.FromFile("ADVeRTISING.png");
                AdvertisingButton.AddSubview(AdvertisingImage);
                AdvertisingButton.TouchUpInside += (object sender, EventArgs e) =>
                {

                    url = "http://www.dnartstudio.it/portfolio/advertising.html";
                    this.PresentModalViewController(wp, false);

                };

                UIButton GraficaButton = new UIButton(new CGRect(PaddingX + PaddingX + widthImage, PaddingY + PaddingY + heightImage + heightText, widthImage, heightImage));
                UIImageView GraficaImage = new UIImageView(GraficaButton.Bounds);
                GraficaImage.Image = UIImage.FromFile("3d.png");
                GraficaButton.AddSubview(GraficaImage);
                GraficaButton.TouchUpInside += (object sender, EventArgs e) =>
                {

                    url = "http://www.dnartstudio.it/portfolio/grafica-3d.html";
                    this.PresentModalViewController(wp, false);

                };

                UILabel AdvertisingText = new UILabel(new CGRect(PaddingX, PaddingY + PaddingY + heightImage + heightImage + heightText, widthText, heightText));
                AdvertisingText.Text = "ADVERTISING";
                AdvertisingText.Font = UIFont.FromName("Roboto-Bold", 17);
                AdvertisingText.TextColor = UIColor.Black;
                AdvertisingText.TextAlignment = UITextAlignment.Center;

                UILabel GraficaText = new UILabel(new CGRect(PaddingX + PaddingX + widthText, PaddingY + PaddingY + heightImage + heightImage + heightText, widthText, heightText));
                GraficaText.Text = "GRAFICA 3D";
                GraficaText.Font = UIFont.FromName("Roboto-Bold", 17);
                GraficaText.TextColor = UIColor.Black;
                GraficaText.TextAlignment = UITextAlignment.Center;

                MainView.Add(AboutButton);
                MainView.Add(StrategiaButton);
                MainView.Add(AboutText);
                MainView.Add(StrategiaText);
                MainView.Add(AdvertisingButton);
                MainView.Add(GraficaButton);
                MainView.Add(AdvertisingText);
                MainView.Add(GraficaText);
            }

        }

    }
}
