﻿using CoreLocation;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using UIKit;

namespace DNArt
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        CLLocationManager locationmanager;
        NSUuid beaconUUID;
        CLBeaconRegion beaconRegion;
        const string beaconId = "BlueBeacon";
        const string uuid = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
        public Dictionary<string, bool> flagBeacon = new Dictionary<string, bool>();
        public Dictionary<string, Beacon> ListBeacon = new Dictionary<string, Beacon>();
        public List<Messaggio> ListMessaggi = new List<Messaggio>();
        bool Background = false;

        public bool Login = false;
        public string PushToken = "";
        public override UIWindow Window
        {
            get;
            set;
        }

        public static AppDelegate Instance { get; private set; }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method
            AppDelegate.Instance = this;

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }


            if (launchOptions != null && launchOptions.Keys != null && launchOptions.Keys.Length != 0 && launchOptions.ContainsKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")))
            {
                Console.WriteLine("entro");
                NSDictionary userInfo = launchOptions.ObjectForKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")) as NSDictionary;
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                Console.WriteLine("a");
                if (userInfo != null)
                {
                    string title = string.Empty;
                    string description = string.Empty;
                    string image_url = string.Empty;
                    string content_url = string.Empty;


                    if (userInfo.ContainsKey(new NSString("title")))
                        title = (userInfo[new NSString("title")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("description")))
                        description = (userInfo[new NSString("description")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("image_url")))
                    {
                        string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/dnart/";
                        string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                        string key2 = "";
                        if (urlimg2.Contains("image"))
                        {
                            key2 = urlimg2.Remove(0, 7);
                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                        }
                        else
                        {
                            key2 = "null";
                            baseurl = "";
                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                        }
                        image_url = baseurl + key2;
                        Console.WriteLine(baseurl + key2);
                    }
                    if (userInfo.ContainsKey(new NSString("content_url")))
                        content_url = (userInfo[new NSString("content_url")] as NSString).ToString();

                    Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url);

                    NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotificationDNArt");
                    NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotificationDNArt");
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtPushNotification");

					UIStoryboard storyboard = new UIStoryboard();
					storyboard = UIStoryboard.FromName("Main", null);

					//UIViewController lp = storyboard.InstantiateViewController("Notification");
					//application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
					//TopController().PresentModalViewController(lp, true);
                }
            }

            startBeacon();

            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            Console.WriteLine("Back:" + NSUserDefaults.StandardUserDefaults.BoolForKey("TokenDNArt"));
            Background = true;
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }

            Console.WriteLine("PUSH TOKEN2:" + DeviceToken);
            //AppDelegate.Instance.PushToken = DeviceToken;

            PushToken = DeviceToken;

            if (Login)
            {
                //******** INVIO TOKEN PUSH *******
                try
                {
                    var clientPush = new RestClient("http://api.netwintec.com:" + 83 + "/");
                    //client.Authenticator = new HttpBasicAuthenticator(username, password);

                    var requestN4UPush = new RestRequest("notification", Method.POST);
                    requestN4UPush.AddHeader("content-type", "application/json");
                    requestN4UPush.AddHeader("Net4U-Company", "dnart");
                    requestN4UPush.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenDNArt"));
                    requestN4UPush.Timeout = 60000;

                    JObject oJsonObject = new JObject();

                    oJsonObject.Add("uuid", PushToken);
                    oJsonObject.Add("type", "ios");

                    requestN4UPush.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                    clientPush.ExecuteAsync(requestN4UPush, null);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Aaaa");
                }
                //************************
            }


            //Save new device token 
            //NSUserDefaults.StandardUserDefaults.SetString(DeviceToken, "PushDeviceToken");
        }


        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            //This method gets called whenever the app is already running and receives a push notification
            // YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
            // this includes setting the badge, playing a sound, etc.

            Console.WriteLine("remote" + application.ApplicationState.ToString());
            if (application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {
                if (Background == false)
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
                    Console.WriteLine("a");
                    if (userInfo != null)
                    {
                        string title = string.Empty;
                        string description = string.Empty;
                        string image_url = string.Empty;
                        string content_url = string.Empty;


                        if (userInfo.ContainsKey(new NSString("title")))
                            title = (userInfo[new NSString("title")] as NSString).ToString();
                        if (userInfo.ContainsKey(new NSString("description")))
                            description = (userInfo[new NSString("description")] as NSString).ToString();
                        if (userInfo.ContainsKey(new NSString("image_url")))
                        {
                            string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/dnart/";
                            string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                            string key2 = "";
                            if (urlimg2.Contains("image"))
                            {
                                key2 = urlimg2.Remove(0, 7);
                                Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                            }
                            else
                            {
                                key2 = "null";
                                baseurl = "";
                                Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                            }
                            image_url = baseurl + key2;
                            Console.WriteLine(baseurl + key2);
                        }
                        if (userInfo.ContainsKey(new NSString("content_url")))
                            content_url = (userInfo[new NSString("content_url")] as NSString).ToString();

                        Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url );

                        NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotificationDNArt");
                        NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotificationDNArt");
                        NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtPushNotification");
                    }
                }
                else
                {

                    Console.WriteLine("");
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                    string title = string.Empty;
                    string description = string.Empty;
                    string image_url = string.Empty;
                    string content_url = string.Empty;


                    if (userInfo.ContainsKey(new NSString("title")))
                        title = (userInfo[new NSString("title")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("description")))
                        description = (userInfo[new NSString("description")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("image_url")))
                    {
                        string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/dnart/";
                        string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                        string key2 = "";
                        if (urlimg2.Contains("image"))
                        {
                            key2 = urlimg2.Remove(0, 7);
                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                        }
                        else
                        {
                            key2 = "null";
                            baseurl = "";
                            Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                        }
                        image_url = baseurl + key2;
                        Console.WriteLine(baseurl + key2);
                    }
                    if (userInfo.ContainsKey(new NSString("content_url")))
                        content_url = (userInfo[new NSString("content_url")] as NSString).ToString();


                    NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotificationDNArt");
                    NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotificationDNArt");
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtPushNotification");

                    Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url);

                    UIStoryboard storyboard = new UIStoryboard();
                    storyboard = UIStoryboard.FromName("Main", null);

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                }

            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                string title = string.Empty;
                string description = string.Empty;
                string image_url = string.Empty;
                string content_url = string.Empty;


                if (userInfo.ContainsKey(new NSString("title")))
                    title = (userInfo[new NSString("title")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("description")))
                    description = (userInfo[new NSString("description")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("image_url")))
                {
                    string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/dnart/";
                    string urlimg2 = (userInfo[new NSString("image_url")] as NSString).ToString();
                    string key2 = "";
                    if (urlimg2.Contains("image"))
                    {
                        key2 = urlimg2.Remove(0, 7);
                        Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                    }
                    else
                    {
                        key2 = "null";
                        baseurl = "";
                        Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                    }
                    image_url = baseurl + key2;
                    Console.WriteLine(baseurl + key2);
                }
                if (userInfo.ContainsKey(new NSString("content_url")))
                    content_url = (userInfo[new NSString("content_url")] as NSString).ToString();

                NSUserDefaults.StandardUserDefaults.SetString(image_url, "ImageNotificationDNArt");
                NSUserDefaults.StandardUserDefaults.SetString(description, "DescriptionNotificationDNArt");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtPushNotification");

                Console.WriteLine("object:" + title + "|" + description + "|" + image_url + "|" + content_url );

                UIStoryboard storyboard = new UIStoryboard();
                storyboard = UIStoryboard.FromName("Main", null);

                UIViewController lp = storyboard.InstantiateViewController("Notification");
                //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                TopController().PresentModalViewController(lp, true);
                //processNotification(userInfo, false);
            }
        }


        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            Console.WriteLine("local" + application.ApplicationState.ToString());
            if (application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {
                if (Background == false)
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtBeaconNotification");

                    UIStoryboard storyboard = new UIStoryboard();
                    storyboard = UIStoryboard.FromName("Main", null);

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                }
                else
                {
                    UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtBeaconNotification");

                    UIStoryboard storyboard = new UIStoryboard();
                    storyboard = UIStoryboard.FromName("Main", null);

                    UIViewController lp = storyboard.InstantiateViewController("Notification");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                }

            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                NSUserDefaults.StandardUserDefaults.SetBool(true, "isDNArtBeaconNotification");
                UIStoryboard storyboard = new UIStoryboard();
                storyboard = UIStoryboard.FromName("Main", null);
                

                UIViewController lp = storyboard.InstantiateViewController("Notification");
                TopController().PresentModalViewController(lp, true);
            }
        }

        public void startBeacon()
        {

            beaconUUID = new NSUuid(uuid);
            beaconRegion = new CLBeaconRegion(beaconUUID, beaconId);


            beaconRegion.NotifyEntryStateOnDisplay = true;
            beaconRegion.NotifyOnEntry = true;
            beaconRegion.NotifyOnExit = true;

            locationmanager = new CLLocationManager();




            locationmanager.RegionEntered += (object sender, CLRegionEventArgs e) => {
                Console.WriteLine("Region Entered");
                //var notification = new UILocalNotification () { AlertBody = "Uno stand è vicino!" };
                //UIApplication.SharedApplication.CancelAllLocalNotifications();
                //UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
            };


            locationmanager.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) => {

               
               if (e.Beacons.Length > 0)
               {
                    for (int a = 0; a < e.Beacons.Length; a++)
                    {
                        var beacon = e.Beacons[a];
                        Console.WriteLine(a + "   " + beacon.Major.ToString() + "  " + beacon.Minor.ToString() + "  " + beacon.Rssi);
                        string App2;
                        List<string> BeacAppoggioCount = new List<string>();

                        int countBeac = 0;
                        for (int q = 0; q < ListMessaggi.Count; q++)
                        {
                            App2 = beacon.Major.ToString() + "," + beacon.Minor.ToString() + "," + q;
                            if (flagBeacon.ContainsKey(App2) == true)
                            {

                                BeacAppoggioCount.Add(App2);
                                countBeac++;

                            }
                        }
                        Console.WriteLine(countBeac);
                        if (countBeac != 0)
                        {
                            for (int w = 0; w < countBeac; w++)
                            {
                                Beacon BeaconApp;
                                BeaconApp = ListBeacon[BeacAppoggioCount[w]];

                                if (ListMessaggi[BeaconApp.IdMessaggio].Visto == false)
                                {
                                    nint distRssi = 0;
									Console.WriteLine(BeaconApp.Distance);

                                    if (BeaconApp.Distance <= 50)
                                        distRssi = (int)(-60);

                                    if (BeaconApp.Distance > 50 && BeaconApp.Distance <= 100)
                                        distRssi = (int)(-67);

                                    if (BeaconApp.Distance > 100 && BeaconApp.Distance <= 150)
                                        distRssi = (int)(-75);

                                    if (BeaconApp.Distance > 150 && BeaconApp.Distance <= 200)
                                        distRssi = (int)(-82);

                                    if (BeaconApp.Distance > 200 && BeaconApp.Distance <= 500)
                                        distRssi = (int)(-90);
									
									if (BeaconApp.Distance > 500)
										distRssi = (int)(-99);

                                    //Console.WriteLine(distRssi + "|" + beacon.Rssi + "|" + (beacon.Rssi > distRssi));
                                    if (beacon.Rssi == 0)
                                        distRssi = (int)(200);

                                    if (beacon.Rssi > distRssi)
                                    {
                                        Messaggio MessaggioApp;
                                        MessaggioApp = ListMessaggi[BeaconApp.IdMessaggio];
                                        flagBeacon[BeacAppoggioCount[w]] = true;
                                        ListMessaggi[BeaconApp.IdMessaggio].Visto = true;

                                        var notification = new UILocalNotification() { AlertBody = MessaggioApp.Titolo, AlertTitle = "Dnart" };
                                        notification.SoundName = UILocalNotification.DefaultSoundName;
                                        UIApplication.SharedApplication.ApplicationIconBadgeNumber = 1;
                                        UIApplication.SharedApplication.CancelAllLocalNotifications();
                                        UIApplication.SharedApplication.PresentLocalNotificationNow(notification);

                                        NSUserDefaults.StandardUserDefaults.SetString(MessaggioApp.UrlImage, "ImageNotificationDNArt");
                                        NSUserDefaults.StandardUserDefaults.SetString(MessaggioApp.Descrizione, "DescriptionNotificationDNArt");

                                        Console.WriteLine(MessaggioApp.Titolo + "   " + MessaggioApp.Descrizione);
                                        w = ListMessaggi.Count + 1;
                                        a = e.Beacons.Length + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                locationmanager.RequestAlwaysAuthorization();
            locationmanager.StartMonitoring(beaconRegion);
            locationmanager.StartRangingBeacons(beaconRegion);

        }

        public UIViewController TopController()
        {
            UIViewController topController = (UIApplication.SharedApplication).KeyWindow.RootViewController;
            while (topController.PresentedViewController != null)
            {
                topController = topController.PresentedViewController;
            }

            return topController;
        }
    }

    public class Beacon
    {

        public string UUID;
        public int Major;
        public int Minor;
        public int Distance;
        public int IdMessaggio;

        public Beacon(string uuid, int major, int minor, int distance, int idmex)
        {
            UUID = uuid;
            Major = major;
            Minor = minor;
            Distance = distance;
            IdMessaggio = idmex;
        }

    }

    public class Messaggio
    {

        public string Titolo;
        public string UrlImage;
        public string Descrizione;
        public string Action;
        public bool Visto;

        public Messaggio(string titolo, string urlimg, string descr, string action, bool visto)
        {
            Titolo = titolo;
            UrlImage = urlimg;
            Descrizione = descr;
            Action = action;
            Visto = visto;
        }

    }


    /***********

    var client = new RestClient("http://api.netwintec.com/");
                        var requestMessage = new RestRequest("active-messages", Method.GET);
                        requestMessage.AddHeader("content-type", "application/json");
                        requestMessage.AddHeader("Net4U-Company", "azienda");
                        requestMessage.AddHeader("Net4U-Token", App.Instance.TokenN4U);

                        IRestResponse response = client.Execute(requestMessage);

                        //\	string prova = "{\"messages\":[{\"title\":\"titolo\",\"description\":\"descrizione\",\"image_url\":\"/image/4fae906a-0e25-4f1d-953c-6d8cf1cc0219\",\"content_url\":\"url\",\"type\":\"ibeacon\",\"timestamp_start\":1429800825,\"timestamp_end\":1450882425,\"distance\":5,\"_type\":\"message\",\"_key\":\"message::e68461b7-13f9-4296-856e-3bf83ffd587a\"}]}";
                        JsonValue json = JsonValue.Parse(response.Content);
                        Console.WriteLine(response.Content);
                        JsonValue data = json["messages"];

                        foreach (JsonValue dataItem in data)
                        {
                            var titolo = dataItem["title"];
                            var urlimg = dataItem["image_url"];
                            var descr = dataItem["description"];
                            var action = dataItem["content_url"];
                            var distanzaAp = dataItem["threshold"];
                            float distanzaFl = 0;
                            if (distanzaAp.ToString().Contains("."))
                            {
                                string distStr = distanzaAp.ToString().Replace('.', ',');
                                distanzaFl = float.Parse(distStr);
                            }
                            else
                            {
                                distanzaFl = float.Parse(distanzaAp.ToString());
                            }
                            Console.WriteLine(distanzaFl);
                            int distanza = (int)(distanzaFl * 100);
                            Console.WriteLine(distanza);
                            var key = dataItem["_key"];

                            Console.WriteLine(titolo + "  " + urlimg + "   " + descr + "   " + action);

                            ListMessaggi.Add(new Messaggio(titolo, urlimg, descr, action, false));

                            var client2 = new RestClient("http://api.netwintec.com/");
                            var requestBeacon = new RestRequest("active-messages/" + key, Method.GET);
                            requestBeacon.AddHeader("content-type", "application/json");
                            requestBeacon.AddHeader("Net4U-Company", "azienda");
                            requestBeacon.AddHeader("Net4U-Token", App.Instance.TokenN4U);

                            IRestResponse response2 = client2.Execute(requestBeacon);

                            Console.WriteLine(response2.Content);

                            //string prova2 = "{\"beacons\":[{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 5},{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 4}]}";

                            JsonValue json2 = JsonValue.Parse(response2.Content);
                            JsonValue data2 = json2["beacons"];

                            foreach (JsonValue dataItem2 in data2)
                            {
                                var uuid2 = dataItem2["uuid"];
                                var majorAp = dataItem2["major"];
                                int major = int.Parse(majorAp.ToString());
                                var minorAp = dataItem2["minor"];
                                int minor = int.Parse(minorAp.ToString());
                                Console.WriteLine(uuid2 + "|" + major + "|" + minor + "|" + distanza);
                                string Appoggio;
                                Appoggio = major.ToString() + "," + minor.ToString() + "," + i;
                                ListBeacon.Add(Appoggio, new Beacon(uuid2, major, minor, distanza, i));
                                flagBeacon.Add(Appoggio, false);
                                if (flagBeacon.ContainsKey(Appoggio) == true)
                                {
                                    Console.WriteLine("c'è");
                                }
                            }
                            i++;
                        }
                        flagTokenN4U = false;
                        Console.WriteLine("Fine");
                    }

    ***********/
}