using CoreAnimation;
using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DNArt
{
	partial class WebPage : UIViewController
	{
        bool flag = false;

		public WebPage (IntPtr handle) : base (handle)
		{
		}
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            //UIImageView image = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - 30, (ContentView.Frame.Height / 2) - 28, 60, 56));
            //image.Image = UIImage.FromFile("pallina.png");


            CGPoint origin = new CGPoint(image.Center.X, image.Center.Y - 50);
            CGPoint target = new CGPoint(image.Center.X, image.Center.Y + 50);


            CABasicAnimation bounce = new CABasicAnimation();
            bounce.KeyPath = "position.y";
            bounce.Duration = 1;
            bounce.From = new NSNumber(origin.Y);
            bounce.To = new NSNumber(target.Y);
            bounce.RepeatCount = float.MaxValue;
            bounce.AutoReverses = true;

            CABasicAnimation rotationAnimation = new CABasicAnimation();
            rotationAnimation.KeyPath = "transform.rotation.z";
            rotationAnimation.To = new NSNumber(Math.PI * 2);
            rotationAnimation.Duration = 2;
            rotationAnimation.Cumulative = true;
            rotationAnimation.RepeatCount = float.MaxValue;
            image.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
            
           
            //image.Layer.AddAnimation(bounce, "Bounce");
            //image.StartAnimating();

            var url = HomePage.Instance.url; // NOTE: https secure request
            web.LoadFinished += delegate {
                if (flag == false)
                {
                    image.Alpha = 0;
                    web.Alpha = 1;
                    image.Layer.RemoveAllAnimations();
                    flag = true;

                }
                //Console.WriteLine("Finish");
            };


            web.LoadRequest(new NSUrlRequest(new NSUrl(url)));
            
            //ContentView.Add(image);
        }
    }
}
