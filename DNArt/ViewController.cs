﻿using CoreAnimation;
using CoreGraphics;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Json;
using System.Text;
using UIKit;

namespace DNArt
{
    public partial class ViewController : UIViewController
    {

        int porta =83;
        int i = 0;
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            UIImageView image = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - 30, (ContentView.Frame.Height / 2) - 28, 60, 56));
            image.Image = UIImage.FromFile("pallina.png");

            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            UIApplication.SharedApplication.CancelAllLocalNotifications();

            CGPoint origin = new CGPoint(image.Center.X, image.Center.Y - 50);
            CGPoint target = new CGPoint(image.Center.X, image.Center.Y + 50);
            CGPoint middle1 = new CGPoint(image.Center.X, image.Center.Y);
            CGPoint middle2 = new CGPoint(image.Center.X, image.Center.Y + 20);


            CABasicAnimation bounce = new CABasicAnimation();
            bounce.KeyPath = "position.y";
            bounce.Duration = 1;
            bounce.From = new NSNumber(origin.Y);
            bounce.To = new NSNumber(target.Y); bounce.RepeatCount = float.MaxValue; bounce.AutoReverses = true;
            /*
            CABasicAnimation bounce1 = new CABasicAnimation();
            bounce1.KeyPath = "position.y";
            bounce1.BeginTime = 1;
            bounce1.Duration = 1;
            bounce1.From = new NSNumber(target.Y);
            bounce1.To = new NSNumber(middle1.Y);

            CABasicAnimation bounce2 = new CABasicAnimation();
            bounce2.KeyPath = "position.y";
            bounce2.BeginTime = 2;
            bounce2.Duration = 1;
            bounce2.From = new NSNumber(middle1.Y);
            bounce2.To = new NSNumber(target.Y);

            CABasicAnimation bounce3 = new CABasicAnimation();
            bounce3.KeyPath = "position.y";
            bounce3.BeginTime = 3;
            bounce3.Duration = 1;
            bounce3.From = new NSNumber(target.Y);
            bounce3.To = new NSNumber(middle2.Y);

            CABasicAnimation bounce4 = new CABasicAnimation();
            bounce4.KeyPath = "position.y";
            bounce4.BeginTime = 4;
            bounce4.Duration = 1;
            bounce4.From = new NSNumber(middle2.Y);
            bounce4.To = new NSNumber(target.Y);

            CAAnimationGroup animationGroup = CAAnimationGroup.CreateAnimation();
            animationGroup.Duration = 5;
            animationGroup.RepeatCount = float.MaxValue;
            animationGroup.Animations = new CAAnimation[] { bounce, bounce1, bounce2, bounce3,bounce4 };*/

            image.Layer.AddAnimation(bounce, null);
            //image.Layer.AddAnimation(animationGroup, null);
            image.StartAnimating();

            /*UIView.Animate(5, 0, UIViewAnimationOptions.CurveEaseOut, () =>
            {
                image.Center = origin;
            }, () =>
            {
                image.Center = target;
            });*/

            /*
           UIDynamicAnimator animator = new UIDynamicAnimator(ContentView);

            UIGravityBehavior gravity = new UIGravityBehavior(image);
            animator.AddBehavior(gravity);

            UICollisionBehavior collision = new UICollisionBehavior(image);
            collision.TranslatesReferenceBoundsIntoBoundary = true;
            animator.AddBehavior(collision);

            UIDynamicItemBehavior elasticity = new UIDynamicItemBehavior(image);
            elasticity.Elasticity= 0.7f;
            animator.AddBehavior(elasticity);

            //image.Layer.AddAnimation(animator,null);*/

            ContentView.Add(image);

            bool firstTime = NSUserDefaults.StandardUserDefaults.BoolForKey("FirstTimeDNArt");
            if (!firstTime || firstTime == null)
            {
                string ID = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();

                var size = UIStringDrawing.StringSize(ID, UIFont.FromName("Roboto-Regular", 15), new CGSize(ContentView.Frame.Width - 60, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                UILabel label = new UILabel(new CGRect(30, 30, ContentView.Frame.Width - 60, size.Height));
                label.Text = ID;
                label.TextColor = UIColor.Black;
                label.Font = UIFont.FromName("Roboto-Regular", 15);
                label.Lines = 0;

                ContentView.Add(label);
                NSUserDefaults.StandardUserDefaults.SetBool(true,"FirstTimeDNArt");
            }
            else
            {
                string ID = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
                Console.WriteLine(ID);
                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U = new RestRequest("login/customer", Method.POST);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "dnart");
                requestN4U.Timeout = 60000;

                JObject oJsonObject = new JObject();

                oJsonObject.Add("email", ID);
                oJsonObject.Add("password", "DNArt");

                requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                client.ExecuteAsync(requestN4U, (s, e) =>
                {

                    var response = s;

                    Console.WriteLine("LOGIN:" + s.StatusCode + "|" + s.Content);

                    if (s.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        InvokeOnMainThread(() =>
                        {


                            AppDelegate.Instance.Login = true;

                            if (!string.IsNullOrWhiteSpace(AppDelegate.Instance.PushToken) && !string.IsNullOrEmpty(AppDelegate.Instance.PushToken))
                            {
                                //******** INVIO TOKEN PUSH *******
                                try
                                {
                                    var clientPush = new RestClient("http://api.netwintec.com:" + 83 + "/");
                                    //client.Authenticator = new HttpBasicAuthenticator(username, password);

                                    var requestN4UPush = new RestRequest("notification", Method.POST);
                                    requestN4UPush.AddHeader("content-type", "application/json");
                                    requestN4UPush.AddHeader("Net4U-Company", "dnart");
                                    requestN4UPush.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenDNArt"));
                                    requestN4UPush.Timeout = 60000;

                                    JObject oJsonObject2 = new JObject();

                                    oJsonObject2.Add("uuid", AppDelegate.Instance.PushToken);
                                    oJsonObject2.Add("type", "ios");

                                    requestN4UPush.AddParameter("application/json; charset=utf-8", oJsonObject2, ParameterType.RequestBody);

                                    clientPush.ExecuteAsync(requestN4UPush, null);

                                }
                                catch (Exception ee)
                                {
                                    Console.WriteLine("Aaaa");
                                }
                                //************************

                            }

                            JsonValue json0 = JsonValue.Parse(response.Content);
                            string token = json0["token"];
                            NSUserDefaults.StandardUserDefaults.SetString(token, "TokenDNArt");

                            UIStoryboard storyboard = new UIStoryboard();
                            storyboard = UIStoryboard.FromName("Main", null);
                            UIViewController lp = storyboard.InstantiateViewController("HomePage");
                            UIViewController lp2 = storyboard.InstantiateViewController("Notification");

                            bool isPushNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isDNArtPushNotification");
                            bool isBeaconNotification = NSUserDefaults.StandardUserDefaults.BoolForKey("isDNArtBeaconNotification");
                            Console.WriteLine("PUSH:" + isPushNotification + "|BEACON:" + isBeaconNotification);

                            if (isPushNotification)
                            {
                                this.PresentModalViewController(lp2, true);

                            }

                            if (isBeaconNotification)
                            {
                                this.PresentModalViewController(lp2, true);
                            }
                            if (!isBeaconNotification && !isPushNotification)
                            {
                                NavigationController.PresentModalViewController(lp, true);
                            }

                            var client2 = new RestClient("http://api.netwintec.com:" + porta + "/");
                            var requestMessage = new RestRequest("active-messages", Method.GET);
                            requestMessage.AddHeader("content-type", "application/json");
                            requestMessage.AddHeader("Net4U-Company", "dnart");
                            requestMessage.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenDNArt"));

                            IRestResponse response2 = client2.Execute(requestMessage);

                            //\	string prova = "{\"messages\":[{\"title\":\"titolo\",\"description\":\"descrizione\",\"image_url\":\"/image/4fae906a-0e25-4f1d-953c-6d8cf1cc0219\",\"content_url\":\"url\",\"type\":\"ibeacon\",\"timestamp_start\":1429800825,\"timestamp_end\":1450882425,\"distance\":5,\"_type\":\"message\",\"_key\":\"message::e68461b7-13f9-4296-856e-3bf83ffd587a\"}]}";
                            JsonValue json = JsonValue.Parse(response2.Content);
                            Console.WriteLine("ACTIVE MESSAGE:" + response2.Content);
                            JsonValue data = json["messages"];

                            foreach (JsonValue dataItem in data)
                            {
                                var titolo = dataItem["title"];
                                var urlimg = dataItem["image_url"];

                                string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/dnart/";
                                string urlimg2 = urlimg;
                                string key2 = "";
                                if (urlimg2.Contains("image"))
                                {
                                    key2 = urlimg2.Remove(0, 7);
                                    Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                                }
                                else
                                {
                                    key2 = "null";
                                    baseurl = "";
                                    Console.WriteLine("IMAGE_URL:" + baseurl + key2);
                                }
                                string imageN4U = baseurl + key2;

                                var descr = dataItem["description"];
                                var action = dataItem["content_url"];
                                var distanzaAp = dataItem["threshold"];
                                float distanzaFl = 0;
                                if (distanzaAp.ToString().Contains("."))
                                {
                                    string distStr = distanzaAp.ToString().Replace('.', ',');
                                    distanzaFl = float.Parse(distStr);
                                }
                                else
                                {
                                    distanzaFl = float.Parse(distanzaAp.ToString());
                                }
                                Console.WriteLine(distanzaFl);
                                int distanza = (int)(distanzaFl * 100);
                                Console.WriteLine(distanza);
                                var key = dataItem["_key"];

                                Console.WriteLine(titolo + "  " + imageN4U + "   " + descr + "   " + action);

                                AppDelegate.Instance.ListMessaggi.Add(new Messaggio(titolo, imageN4U, descr, action, false));

                                var client3 = new RestClient("http://api.netwintec.com:" + porta + "/");
                                var requestBeacon = new RestRequest("active-messages/" + key, Method.GET);
                                requestBeacon.AddHeader("content-type", "application/json");
                                requestBeacon.AddHeader("Net4U-Company", "dnart");
                                requestBeacon.AddHeader("Net4U-Token", token);

                                IRestResponse response3 = client3.Execute(requestBeacon);

                                Console.WriteLine("ACTIVE MESSAGE KEY:" + response3.Content);

                                //string prova2 = "{\"beacons\":[{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 5},{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 4}]}";

                                JsonValue json2 = JsonValue.Parse(response3.Content);
                                JsonValue data2 = json2["beacons"];

                                foreach (JsonValue dataItem2 in data2)
                                {
                                    var uuid2 = dataItem2["uuid"];
                                    var majorAp = dataItem2["major"];
                                    int major = int.Parse(majorAp.ToString());
                                    var minorAp = dataItem2["minor"];
                                    int minor = int.Parse(minorAp.ToString());
                                    Console.WriteLine(uuid2 + "|" + major + "|" + minor + "|" + distanza);
                                    string Appoggio;
                                    Appoggio = major.ToString() + "," + minor.ToString() + "," + i;
                                    AppDelegate.Instance.ListBeacon.Add(Appoggio, new Beacon(uuid2, major, minor, distanza, i));
                                    AppDelegate.Instance.flagBeacon.Add(Appoggio, false);
                                    if (AppDelegate.Instance.flagBeacon.ContainsKey(Appoggio) == true)
                                    {
                                        Console.WriteLine("c'è");
                                    }
                                }
                                i++;
                            }
                            Console.WriteLine("Fine");
                        });
                    }

                });
            }
            /*
            UIButton aa = new UIButton(new CGRect(ContentView.Frame.Width/2-50,ContentView.Frame.Height -50,100,40));
            aa.SetTitle("Premi",UIControlState.Normal);
            aa.SetTitleColor(UIColor.White,UIControlState.Normal);
            aa.BackgroundColor = UIColor.Black;
            aa.TouchUpInside += delegate {
                UIStoryboard storyboard = new UIStoryboard();
                storyboard = UIStoryboard.FromName("Main", null);
                UIViewController lp = storyboard.InstantiateViewController("HomePage");
                NavigationController.PresentModalViewController(lp, true);
            };
            ContentView.Add(aa);

            Console.WriteLine("ID:" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());
            */

            /*
            Roboto-Regular
            Roboto-Bold
            */

            /*
             var fontList = new StringBuilder();
             var familyNames = UIFont.FamilyNames;

             foreach (var familyName in familyNames)
             {
                 fontList.Append(String.Format("Family; {0} \n", familyName));
                 Console.WriteLine("Family: {0}\n", familyName);

                 var fontNames = UIFont.FontNamesForFamilyName(familyName);

                 foreach (var fontName in fontNames)
                 {
                     Console.WriteLine("\tFont: {0}\n", fontName);
                 }

                 // Perform any additional setup after loading the view, typically from a nib.
             }*/

            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
        
    }
}